# -*- coding: utf-8 -*-
import os
from flask import Flask, render_template, flash, request, send_from_directory, send_file, redirect
from wtforms import Form, TextField, TextAreaField, validators, StringField, IntegerField, SubmitField, ValidationError
from werkzeug import secure_filename
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES
import fitz
import re
import string


# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
app.config['UPLOAD_FOLDER'] = 'static/uploads'
ALLOWED_EXTENSIONS = set(['pdf'])
#the name 'datafiles' must match in app.config to DATAFILES
docs = UploadSet('datafiles', DOCUMENTS)
app.config['UPLOADED_DATAFILES_DEST'] = 'static/uploads'
configure_uploads(app, docs)

UPLOAD_DIRECTORY = app.config['UPLOAD_FOLDER']

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/Partha")
def myname():
    return "Hello, Partha"

@app.route("/download")
def download_file():
    for filename in os.listdir(app.config['UPLOAD_FOLDER']):
        if filename.endswith('_out.pdf'):
            # os.unlink(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            try:
                #return send_file(os.path.join(app.config['UPLOAD_FOLDER'], filename), attachment_filename=filename)
                return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True)
            except Exception as e:
                return str(e)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class ReusableForm(Form):
    name = StringField("Name:", validators=[validators.DataRequired("Please mention Input PDF filename with path")])
    fmpc = IntegerField('FMpagecount:', validators=[validators.DataRequired("Please mention the total front matter page count.")])
    isp = IntegerField('Index-start-page:', validators=[validators.DataRequired("Please mention the Index start page number.")])
    iep = IntegerField('Index-end-page:', validators=[validators.DataRequired("Please mention the Index end page number.")])

    @app.route("/linktool", methods=['GET', 'POST'])
    def linktool():
        FolderCleanup(app.config['UPLOAD_FOLDER'])
        form = ReusableForm(request.form)
        print (form.errors)
        if request.method == 'POST':
            if form.validate():
                f = request.files['user_file']
                if f.filename == '':
                    flash('Error: No selected file')
                    return render_template('linktool.html', form=form)
                if allowed_file(f.filename):
                    name = request.form['name']
                    fmpc = request.form['fmpc']
                    isp = request.form['isp']
                    iep = request.form['iep']
                    f.save(os.path.join(app.config['UPLOAD_FOLDER'], f.filename))
                    flash('file uploaded successfully! - ' + f.filename)
                    flash('PDF Name ' + name)
                    flash('Frontmatter Page count ' + fmpc)
                    flash('Index Start PageNo ' + isp)
                    flash('Index End PageNo ' + iep)

                    result = StartLinking(os.path.join(app.config['UPLOAD_FOLDER'], f.filename), fmpc, isp, iep)

                    sOutputFileName = f.filename.replace(".pdf", "_out.pdf")
                    #send_from_directory(app.config['UPLOAD_FOLDER'], sOutputFileName, as_attachment=True)
                    flash(result)
                    #send_file(os.path.join(app.config['UPLOAD_FOLDER'], sOutputFileName), as_attachment=True)
                    #return redirect('/static/uploads/' + sOutputFileName)
                    return render_template('download.html')
                else:
                    flash('Error: Select only PDF File.')
                    return render_template('linktool.html', form=form)
            else:
                flash('Error: All the form fields are required. ')

        return render_template('linktool.html', form=form)

def FolderCleanup(dpath):
    for filename in os.listdir(dpath):
        if filename.endswith('.pdf'):
            os.unlink(os.path.join(app.config['UPLOAD_FOLDER'], filename))

def StartLinking(inputfilename, fmpagecount, indexstartpage, indexendpage):
    doc = fitz.open(inputfilename)
    if indexendpage == None:
        EnableIndexlink(doc, fmpagecount, indexstartpage)
    else:
        for pn in range(int(indexstartpage), int(indexendpage) + 1):
            EnableIndexlink(doc, fmpagecount, str(pn))
            # print(str(pn))
    sOutputFileName = inputfilename.replace(".pdf", "_out.pdf")
    doc.save(sOutputFileName)
    doc.close()
    return ("Index linking enabled!")


def trimEndExtra(dict):
    RectAfter = fitz.Rect(0.0, 0.0, 2.25, 0.0)
    return dict - RectAfter

def captureFirstno(lensuffix,dict):
    SingleCharRect = fitz.Rect(0.0, 0.0, 4.5, 0.0)
    for n in range(0, lensuffix - 1):
        dict = dict - SingleCharRect
    return dict

def captureSecondno(lenpreffix,dict):
    SingleCharRect = fitz.Rect(4.5, 0.0, 0.0, 0.0)
    for n in range(0, lenpreffix + 1):
        dict = dict + SingleCharRect
    return dict

def bulidDict(iPage, rDict, FMPageCount):
    FMPageCount = int(FMPageCount) - 1
    linkPageno = int(iPage) + FMPageCount
    cdict = {'kind': 1,
             'from': rDict,
             'page': linkPageno,
             'to': fitz.Point(0, 665),
             'zoom': float(0)
             }
    return cdict

def EnableIndexlink(doc, FMPageCount, currentPageNo):
    correctPageNo = (int(currentPageNo) + int(FMPageCount)) - 1
    print(correctPageNo)
    page = doc[int(correctPageNo)]
    #f = open("F:\WorkStation\From_Aura\wetransfer-ca0c32\links.txt", "w+")
    wlist = page.getTextWords()
    for w in wlist:
        r = fitz.Rect(w[:4])
        txt = w[4:5]
        #f.write("Text: " + str(txt) + "\n")
        matchNumObj = re.match(r'^([0-9]+)(?:[a-z][0-9]+)?.?$', str(txt[0]), re.S)
        matchNumRangeObj = re.match(r'^([0-9]+)(?:[a-z][0-9]+)?–([0-9]+)(?:[a-z][0-9]+)?.?$', str(txt[0]), re.S)
        if matchNumObj:
            MatchInfo = matchNumObj.group()
            PageNo = matchNumObj.group(1)
            #f.write("Actual: " + MatchInfo + "\n")
            matchNondigitObj = re.search(r'[^0-9]$', MatchInfo, re.M)
            if matchNondigitObj:
                #f.write("Number: " + PageNo + "<=> Org: " + str(r) + ", Final: " + str(trimEndExtra(r)) + "\n")
                adict = bulidDict(PageNo, trimEndExtra(r), FMPageCount)
                page.insertLink(adict)
            else:
                #f.write("Number: " + PageNo + "<=> Org1: " + str(r) + "\n")
                adict = bulidDict(PageNo, r, FMPageCount)
                page.insertLink(adict)
        elif matchNumRangeObj:
            MatchTxt = matchNumRangeObj.group()
            FirstNo = matchNumRangeObj.group(1)
            SecondNo = matchNumRangeObj.group(2)
            matchNonObj = re.search(r'[^0-9]$', MatchTxt, re.M)
            if matchNonObj:
                #f.write("Range1: group1: " + "Match text: " + str(len(MatchTxt) - MatchTxt.find("–")) + ", " + FirstNo + " len: " + str(len(FirstNo)) + " <=> Org: " + str(r) + " |||| " + str(captureFirstno(len(FirstNo), r)) + "\n")
                #f.write("group2: " + SecondNo + " len: " + str(len(SecondNo)) + " <=> Org: " + str(r) + " |||| " + str(captureSecondno(len(SecondNo), trimEndExtra(r))) + "\n")
                adictOne = bulidDict(FirstNo, captureFirstno(len(MatchTxt) - MatchTxt.find("–"), trimEndExtra(r)), FMPageCount)
                page.insertLink(adictOne)
                if int(SecondNo) <= int(FirstNo):
                    dif = len(str(FirstNo)) - len(str(SecondNo))
                    cvalue = str(FirstNo)[:dif] + str(SecondNo)
                else:
                    cvalue = SecondNo
                #f.write(cvalue + "\n")
                adictTwo = bulidDict(cvalue, captureSecondno(MatchTxt.find("–"), trimEndExtra(r)), FMPageCount)
                page.insertLink(adictTwo)
            else:
                #f.write("Range2: group1: " + FirstNo + " <=> Org: " + str(captureFirstno(len(FirstNo), r)) + "\n")
                #f.write("group2: " + SecondNo + " <=> Org: " + str(captureSecondno(len(SecondNo), r)) + "\n")
                adictOne = bulidDict(FirstNo, captureFirstno((len(MatchTxt) - MatchTxt.find("–")) + 1, r), FMPageCount)
                page.insertLink(adictOne)
                if int(SecondNo) <= int(FirstNo):
                    dif = len(str(FirstNo)) - len(str(SecondNo))
                    cvalue = str(FirstNo)[:dif] + str(SecondNo)
                else:
                    cvalue = SecondNo
                    #f.write(cvalue + "\n")
                adictTwo = bulidDict(cvalue, captureSecondno(MatchTxt.find("–"), r), FMPageCount)
                #print(str(adictTwo))
                page.insertLink(adictTwo)
        #else:
            #f.write("OtherText: " + str(txt[0]) + "\n")

    #f.close()
#print(fitz.__doc__)

if __name__ == "__main__":
    app.run(debug=True)